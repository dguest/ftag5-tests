#!/usr/bin/env bash

if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

mkdir -p submit
cd submit

TARBALL=workarea.tar
if [[ ! -f $TARBALL ]]; then
    pathena --trf "Reco_tf.py --outputDAODFile pool.root \
          --inputAODFile %IN --reductionConf FTAG5" \
            --inDS x --outDS user.${USER}.x \
            --outTarBall=$TARBALL --noSubmit
fi

INPUT_DATASETS=( $(cat ../datasets/ftag5-*) )

BATCH_ID=$(date +%F-T%H%M%S)-R${RANDOM}
for DS in ${INPUT_DATASETS[*]}
do
    DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
    OUT_DS=user.${USER}.${DSID}.DAOD_FTAG5.${BATCH_ID}
	  pathena \
	      --trf \
	      "Reco_tf.py \
--preExec 'from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-40\"'\
           --inputAODFile %IN\
           --outputDAODFile pool.root \
           --reductionConf FTAG5"\
        --noEmail\
        --inTarBall=$TARBALL\
        --inDS ${DS} --outDS ${OUT_DS}
done
