# this be -*- bash -*-

Reco_tf.py --preExec "from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-40\""\
           --inputAODFile AOD.pool.root\
           --outputDAODFile TEST.pool.root \
           --maxEvents 10 \
           --reductionConf FTAG5
